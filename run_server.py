import random
from time import sleep

from mesgex.main import ServerApp
from mesgex.tests import run_thread


if __name__ == '__main__':
    threads = []

    s1 = ServerApp(server_listening_addr=('localhost', 60606),
                   client_listening_addr=('localhost', 60607))
    threads.append(run_thread(target=s1.run_service))

    sleep(0.1)

    s2 = ServerApp(server_listening_addr=('localhost', 60608),
                   client_listening_addr=('localhost', 60609),
                   servers_to_connect_to=(('localhost', 60606),))
    threads.append(run_thread(target=s2.run_service))

    sleep(0.1)
