import random

from SimpleWebSocketServer import SimpleWebSocketServer

from mesgex.tests import run_thread
from mesgex.websocket import MesgexWebSocket


class WS1(MesgexWebSocket):
    """
    To define the address of another MESGEX server create a second class ex:
    class WS2(MesgexWebSocket):
        mesgex_server_addr = ('localhost', 60609)
    """
    mesgex_server_addr = ('localhost', 60607)


class WS2(MesgexWebSocket):
    mesgex_server_addr = ('localhost', 60609)


if __name__ == '__main__':
    """
// To connect through JS (assuming port = 60266) use this:
port = 60266

ws1 = new WebSocket('ws://localhost:' + port);
ws1.onmessage = function(ev){console.log('George received: ' + ev.data)};
setTimeout(function(){ ws1.send('600 6 George') }, 100);
setTimeout(function(){ ws1.send('201 0 ') }, 300);

ws2 = new WebSocket('ws://localhost:' + port);
ws2.onmessage = function(ev){console.log('James received: ' + ev.data)};
setTimeout(function(){ ws2.send('600 5 James') }, 500);
setTimeout(function(){ ws2.send('201 0 ') }, 600);

setTimeout(function(){ ws2.send('250 51 James:George:Test message for George of the jungle.') }, 1000);
// George's confirmation of receiving James' message is still missing.
    """
    threads = []

    # port = random.randint(10000, 65000)
    port = 64214

    s1 = SimpleWebSocketServer('', port, WS1)

    threads.append(run_thread(target=s1.serveforever))

    s2 = SimpleWebSocketServer('', port + 1, WS2)

    s2.serveforever()
